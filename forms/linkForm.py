from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField
from wtforms.validators import DataRequired, URL

class LinkForm(FlaskForm):
    link = StringField('Link', validators=[DataRequired(), URL()])
    display_info = BooleanField('Edit information', default="checked")
    submit = SubmitField('Convert Link')
