Player is a small flask server that allows to download mp3 files from any website supported by youtubedl library.
While the file is converting to mp3, you can edit the id3 tags of the  file.

To run the server you need:
- Docker
- Docker compose

Start the server with:
`docker-compose start`