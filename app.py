from __future__ import unicode_literals
import youtube_dl
import os
from config import Config
import tasks
from forms.linkForm import LinkForm
from forms.id3Form import Id3Form
from werkzeug.datastructures import MultiDict
from flask import Flask, make_response, render_template, request, g, send_file, abort
from mutagen.easyid3 import EasyID3

app = Flask(__name__)
app.config.from_object(Config)
celery = tasks.make_celery(app)

DOWNLOAD_PATH = "/app/static/music"


@app.route('/', methods=["GET", "POST"])
def index():
    link_form = LinkForm(request.form)
    if request.method == 'POST' and link_form.validate():
        info = download_info(request.form.get('link', None))
        if info == None:
            link_form.link.errors.append("Link is not supported")
        else:
            # Start Async task to convert video
            id = convert_video.delay(request.form.get('link', None))
            id = id.task_id
            if request.form.get('display_info') != None:
                id3_form = Id3Form(info)
                return render_template("download.html", link_form=link_form, id3_form=id3_form, id=id)
            else:
                return render_template("download.html", link_form=link_form, id=id, info=info)
    return render_template("link_form.html", link_form=link_form)


@app.route('/download/<id>', methods=["GET", "POST"])
def download_file(id):
    process = convert_video.AsyncResult(id)
    if process.ready():
        file_path = process.result
        # Check if we need the ID3 metatags needs some edit
        if request.method == "POST":
            id3_form = Id3Form(request.form)
            if id3_form.validate():
                song = EasyID3(file_path)
                song['title'] = request.form.get('title', None)
                song['artist'] = request.form.get('artist', None)
                song.save()
        try:
            res = send_file(file_path, as_attachment=True)
            return res
        except Exception as e:
            app.logger.error(e)
    return abort(404)


'''
    Get the status of the video convertion process in order to know whether we can download the file or not
'''
@app.route('/download/<id>/status')
def get_convert_status(id):
    file = convert_video.AsyncResult(id)
    return file.status


'''
	download_info extracts the information about the downloaded file. It also
	act as a validation whether the wanted link is supported by youtube_dl or not.
	If the link is unsupported download_info returns None.
'''

def download_info(links):
    with youtube_dl.YoutubeDL({'socket_timeout': 2}) as ydl:
        try:
            info = ydl.extract_info(links, download=False)
        except Exception as e:
            print(e)
            return None
    return MultiDict([('title', info['alt_title']), ('artist', info['creator'])])

'''
    convert_video starts the process of extracting the video and converting it into mp3.
    The process is asynchronous and is done by Celery.
    If no problem occurs, it returns the path where the file will be available. Otherwise it returns None
'''

@celery.task(name='app.convert_video')
def convert_video(links):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': os.path.join(DOWNLOAD_PATH, '%(id)s.%(ext)s'),
        'noplaylist': True,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
        }]
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        try:
            info = ydl.extract_info(links)
            file_path = os.path.join(DOWNLOAD_PATH, info.get('id') + '.mp3')
        except Exception as e:
            return (None)
        return file_path


@app.errorhandler(401)
@app.errorhandler(404)
@app.errorhandler(500)
def real_404(error):
    return "Real 404 page", 404


if __name__ == "__main__":
    app.run(host='0.0.0.0')
