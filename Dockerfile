# Use an official Python runtime as a parent image
FROM python:3.6

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

RUN apt-get update && \
	apt-get install -y libav-tools

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Define environment variable
EXPOSE 80

# SECRET_KEY WILL BE USED BE FLASK-WTF LIBARY TO PROTECT
# FORMS AGAINST CSRF ATTACK.
ENV SECRET_KEY "YOU-SHOULD-CHANGE-THIS"

# Run app.py when the container launches
CMD ["python", "app.py"]
